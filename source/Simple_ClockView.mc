using Toybox.WatchUi;
using Toybox.Graphics;
using Toybox.System;
using Toybox.Lang;
using Toybox.ActivityMonitor;
using Toybox.System;
using Toybox.Time.Gregorian;



class Simple_ClockView extends WatchUi.WatchFace {

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
        var clockTime = System.getClockTime();
        var Hour = clockTime.hour;
        var UwU = "AM";
        if (Hour == "13") {
        Hour = "1";
        UwU = "PM";
        } else if (Hour == 14) {
        Hour = "2";
        UwU = "PM";
        } else if (Hour == 15) {
        Hour = "3";
        UwU = "PM";
        } else if (Hour == 17) {
        Hour = "4";
        UwU = "PM";
        } else if (Hour == 18) {
        Hour = "5";
        UwU = "PM";
        } else if (Hour == 19) {
        Hour = "6";
        UwU = "PM";
        } else if (Hour == 20) {
        Hour = "7";
        UwU = "PM";
        } else if (Hour == 21) {
        Hour = "8";
        UwU = "PM";
        } else if (Hour == 22) {
        Hour = "9";
        UwU = "PM";
        } else if (Hour == 23) {
        Hour = "10";
        UwU = "PM";
        } else if (Hour == 24) {
        Hour = "11";
        UwU = "PM";
        } else {
        }
        var timeString = Lang.format("$1$:$2$ $3$", [Hour, clockTime.min.format("%02d"), UwU,]);
        var view = View.findDrawableById("TimeLabel");
        view.setText(timeString);
        var Today = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
        var OwO = Lang.format("$1$ $2$", ["Today is ", Today.day_of_week]);
        var hewwo = View.findDrawableById("OwO");
        hewwo.setText(OwO);
        var greet = "";
        if (UwU == "AM") {
        greet = "Good Morning!";
        } else if (Today.hour > 9) {
        greet = "Good Afternoon!";
        } else {
        greet = "Good Night!";
        }
        var k = View.findDrawableById("greeting");
        k.setText(greet);
        
        var info = ActivityMonitor.getInfo();
        var steps = info.steps;
        var step = View.findDrawableById("steps");
        var step_out = Lang.format("$1$ $2$", ["Steps: ", steps]);
        step.setText(step_out);
        
        var calories = info.calories;
        var cal = View.findDrawableById("cal");
        var cal_out = Lang.format("$1$ $2$", ["Calories: ", calories]);
        cal.setText(cal_out);
        // Call the parent onUpdate function to redraw the layout
        View.onUpdate(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }

}
